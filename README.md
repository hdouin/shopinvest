# shopinvest


Mini site évaluation de compétences sur le framework
Laravel et ses dépendences. Il se compose d'une page d'accueil et d'une produit en FrontOffice et des méthodes CRUD  pour la table Produit en backOffice. La liste des technologies ci-dessous sont necessaires à son fonctionnement.

- Systeme [Ubuntu v18.04.4 LTS](https://releases.ubuntu.com/18.04.4/)
- Serveur local [Apache v2.4.29](http://httpd.apache.org/docs/2.4/fr/install.html)
- Language serveur [PHP v7.2.24](https://php.com/)
- Language de base de données [MySQL v5.7.29](https://mysql.com/)
- Framework [Laravel v7.0](https://laravel.com/)

## **Environnement**

### Serveur de base de données

- Serveur : Localhost via UNIX socket
- Type de serveur : MySQL
- Version du serveur : 5.7.29-0ubuntu0.18.04.1 - (Ubuntu)
- Version du protocole : 10
- Jeu de caractères : UTF-8 Unicode (utf8)

### Serveur web
- Apache/2.4.29 (Ubuntu)
- MySQL client de base de données : libmysql - mysqlnd 5.0.12-dev
- PHP : 7.2.24-0ubuntu0.18.04.4 (extension mysqli curl mbstring)

## **Installation**

### LINUX APACHE MYSQL PHP (LAMP)

>LAMP est un acronyme désignant un ensemble de logiciels libres permettant de construire des serveurs de sites web. L'acronyme original se réfère aux logiciels suivants : « Linux », le système d'exploitation ; « Apache », le serveur Web ; « MySQL ou MariaDB », le serveur de base de données ; « PHP », le language serveur. [Wikipédia](https://https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=26&cad=rja&uact=8&ved=2ahUKEwj_keuc_4DpAhUT8uAKHU80AvMQmhMwGXoECBIQAg&url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FLAMP&usg=AOvVaw2TY7ClH1ozb7M3rOc0mA2g)

Mise à jour système

```
sudo apt-get update
```

Installation MySQL
```
sudo apt-get install mysql-server mysql-client libmysqlclient-dev
```
Installation Apache
```
sudo apt-get install apache2 -y
sudo systemctl start apache2.service
```
Installation PHP
```
sudo apt-get install php -y
sudo apt-get install php-{bcmath,bz2,intl,gd,mbstring,mysql,zip,fpm} -y
sudo systemctl restart apache2.service
```
Installation phpMyAdmin (administration MySQL)
```
sudo apt-get install phpmyadmin
sudo systemctl restart apache2.service
```


### COMPOSER
> Composer est un logiciel gestionnaire de dépendances libre écrit en PHP. Il permet à ses utilisateurs de déclarer et d'installer les bibliothèques dont le projet principal a besoin. Le développement a débuté en avril 2011 et a donné lieu à une première version sortie le 1ᵉʳ mars 2012. [Wikipédia](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=30&cad=rja&uact=8&ved=2ahUKEwjgm7rl-YDpAhVDzBoKHfYhCS8QmhMwHXoECBUQAg&url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FComposer_(logiciel)&usg=AOvVaw0L-9MIQxAqEpRSAwHbN9io)

Pour installer Composer sur Ubuntu v18.04 LTS

```
sudo apt install composer
```

### LARAVEL

> Laravel est un framework web open-source écrit en PHP respectant le principe modèle-vue-contrôleur et entièrement développé en programmation orientée objet. Laravel est distribué sous licence MIT, avec ses sources hébergées sur GitHub. [Wikipédia](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=30&cad=rja&uact=8&ved=2ahUKEwjPjPqVjYHpAhWH2BQKHYqmBe4QmhMwHXoECBUQAg&url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FLaravel&usg=AOvVaw36ZwFYDexFdmavatCGV2b1)

Pour installer le framework Laravel
```
composer global require laravel/installer
```

### APACHE

> Configuration des fichiers **hosts** et **vhost** pour Apache.

Ajoutez l'adresse du site web dans les **hosts** du système
```
sudo nano /etc/hosts
127.0.0.1    shopinvest.local
```
Créez le fichier **vhost** pour le projet shopinvest
puis ajoutez le dans la configuration de Apache et enfin redémarrez le serveur
```
cd /etc/apache2/sites-available

sudo nano shopinvest.conf

<VirtualHost *:80>
    ServerName shopinvest.local
    DocumentRoot /var/www/html/shopinvest/public

    <Directory /var/www/html/shopinvest/public>
            AllowOverride All
            Order Allow,Deny
            Allow from all
    </Directory>
</VirtualHost>

sudo a2ensite shopinvest.conf

sudo systemctl restart apache2.service
```

## **LE PROJET**

> Le projet devient fonctionnel soit:
>
>en récupérant le fichier shopinvest.zip qui est une archive compressée d'une installation complète du framework LARAVEL en version 7.0 sans le dossier Vendor de dépendences de LARAVEL.
>
>ou
>
>en récupérant le dépot depuis GitLab

Décompressez l'archive dans le dossier suivant puis mettre à jour le dossier des dépendences Vendor de LARAVEL
```
cd /var/www/html
tar -xvf shopinvest.zip

cd /shopinvest
composer update
```

ou
faire un clone depuis le dépot GitLab puis mettre à jour le dossier des dépendences Vendor de LARAVEL
```
cd /var/www/html
git clone https://gitlab.com/hdouin/shopinvest.git

cd /shopinvest
composer update
```

Attribuez les droits aux répertoires suivants
```
cd /var/www/html/shopinvest
sudo chmod -R 775 storage
sudo chmod -R 775 bootstrap/cache
```

Ajoutez la base de donnée
```
cd /var/www/html/shopinvest/sources
gunzip shopinvest.sql.gz

mysql> CREATE DATABASE shopinvest;
mysql> use shopinvest
mysql> mysql shopinvest < shopinvest.sql
```
