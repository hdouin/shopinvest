<footer class="footer" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                © 2020 shopinvest.com
            </div>
            <div class="col-xs-4">
                <ul>
                    <li><a href="http://h.douin.free.fr" target="_blank">Webmaster: h.douin</a></li>
                    <li><a href="#" title="plan de site">Plan de Site</a></li>
                </ul>
            </div>
            <div class="col-xs-4">
                <a class="fa fa-facebook-square" target="_blank" href="#"></a>
                <a class="fa fa-linkedin-square" target="_blank" href="#"></a>
                <a class="fa fa-twitter-square" target="_blank" href="#"></a>
                <a class="fa fa-google-plus-square" target="_blank" href="#"></a>
                <a class="fa fa-youtube-square" target="_blank" href="#"></a>
            </div>
        </div>
    </div>
</footer>
