<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Laravel') }}</title>
<title>Shopinvest</title>

<link href="//getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="//fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="/css/styles.css" rel="stylesheet">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="/js/default.js"></script>
