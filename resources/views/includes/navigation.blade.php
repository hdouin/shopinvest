<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">&nbsp;</span>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav nav-pills">
                @foreach ($navigation as $nav)
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="product" role="button" aria-haspopup="true" aria-expanded="false">{{ $nav['name'] }}</a>
                    <div class="dropdown-menu">
                        @foreach ($nav['children'] as $child)
                        <a class="dropdown-item" href="{{ '/catalogue/products/'.$nav['id'].'/product/'.$child->id }}">{{ $child->name }}</a>
                        @endforeach
                    </div>
                </li>
                @endforeach
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin</a>
                    <div class="dropdown-menu">
                        @foreach (['product','categories','paiements','livraisons'] as $item)
                        <a class="dropdown-item" href="{{ '/admin/catalogue/'.$item.'/list' }}">{{ ucfirst($item) }}</a>
                        @endforeach
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
