<header class="row" id="header">
    <div class="jumbotron" id="jumbotron">
        <div class="row">
            <div class="col-md-3">
                <div class="logo">
                    <a href="/">Shopinvest</a>
                </div>
            </div>
            <div class="col-md-7">
            </div>
            <div class="col-md-2">
                @if (Request::is('catalogue/*'))
                <div id="panier">
                    <div class="panier">
                        <span class="fa-shopping-cart-txt">Mon Panier</span>
                    </div>
                    <div class="panier">
                        <span class="article">0 article</span> | <span class="total">0</span>&nbsp;<span class="devise">&euro;</span>
                    </div>
                </div>
                @endif
                @if (Request::is('/'))
                    @if (Route::has('login'))
                    <div class="top-right links">
                        <button type="button" id="btn-login" class="btn btn-primary">{{ __('Login') }}</button>
                    </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
</header>
