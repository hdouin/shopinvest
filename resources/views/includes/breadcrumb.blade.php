@if (Request::is('catalogue/*'))
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Catalogue</a></li>
        <li class="breadcrumb-item"><a href="#">Products-1</a></li>
        <li class="breadcrumb-item active" aria-current="page">Product-1</li>
    </ol>
</nav>    
@endif