<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('includes.head')
        @yield('javascript')
    </head>
    <body>
        <div class="container">
            <header class="row" id="header">
                @include('includes.header')
                @include('auth.login')
            </header>
            @include('includes.navigation')
            @include('includes.breadcrumb')
            @yield('container')
            @include('includes.footer')
            <a id="back-top" href="#" class="btn btn-primary btn-lg" role="button" data-toggle="tooltip" data-placement="left">
                <span class="glyphicon glyphicon-chevron-up"></span>
            </a>
        </div>
    </body>
</html>
