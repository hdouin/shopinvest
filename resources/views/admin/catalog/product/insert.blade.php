@extends('master')

@section('container')
<div class="container-fluid">
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Insert<a class="float-right" href="/admin/catalogue/product/list">Retour</a></h2>
                    <hr>
                </div>
                <div class="col-md-12">
                    <form action="/admin/catalogue/product/insert" method="POST" enctype="application/form-data">
                        <fieldset>
                            <legend>Product</legend>
                            <div class="form-row">
                                <label for="id_categorie">Categorie</label>
                                <select name="id_categorie">
                                    @foreach ($categories as $val)
                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row">
                                <label for="id_marque">Marque</label>
                                <select name="id_marque">
                                    @foreach ($marques as $val)
                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row">
                                <label for="id_livraison">Livraison</label>
                                <select name="id_livraison">
                                    @foreach ($livraisons as $val)
                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row">
                                <label for="id_paiement">Paiement</label>
                                <select name="id_paiement">
                                    @foreach ($paiements as $val)
                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <label for="rate">Note</label>
                                <select name="rate">
                                    @foreach ([1,2,3,4,5] as $val)
                                    <option value="{{ $val }}">{{ $val }} étoile</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="" placeholder="Product">
                            </div>
                            <div class="form-row">
                                <label for="presentation">Presentation</label>
                                <textarea name="presentation" placeholder="Texte de présentation"></textarea>
                            </div>
                            <div class="form-row">
                                <label for="description">Description</label>
                                <textarea name="description" placeholder="Texte de description"></textarea>
                            </div>
                            <div class="form-row">
                                <label for="garantie">Garantie</label>
                                <textarea name="garantie" placeholder="Texte de garantie"></textarea>
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <label for="prix_barre">Prix barre</label>
                                <input type="text" name="prix_barre" placeholder="0.00" value="">
                            </div>
                            <div class="form-row">
                                <label for="prix_vente">Prix vente</label>
                                <input type="text" name="prix_vente" placeholder="0.00" value="">
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <input class="btn btn-primary" type="submit">
                            </div>
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection