@extends('master')

@section('javascript')
<script src="/js/product.js"></script>
@endsection

@section('container')
<div class="container-fluid">
    <div class="content-wrapper">
        <div class="container">
            <div id="" class="row">
                <div class="col-md-12">
                    <h2>Admin products</h2>
                </div>
                <div class="col-md-12">
                    @if(session()->get('message'))
                        <div class="alert alert-success">
                           {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
                <div class="col-md-12">
                    <table class="table" with="100%">
                        <thead>
                            <tr>
                                <th>Created at</th>
                                <th>Updated at</th>
                                <th>Category</th>
                                <th>Name</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rows as $row)
                            <tr>
                                <td width="150">{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
                                <td width="150">{{ date('d/m/Y', strtotime($row->updated_at)) }}</td>
                                <td width="200">{{ $categories[$row->id_categorie]['name'] }}</td>
                                <td><b>{{ $row->name }}<b></td>
                                <td>
                                    @for ($i=1; $i<6; $i++)
                                    @if ($i<=$row->rate)
                                    <i class="fa fa-star gold"></i>
                                    @else
                                    <i class="fa fa-star-o"></i>
                                    @endif
                                    @endfor
                                </td>
                                <td><a href="{{ '/admin/catalogue/product/update/'.$row->id }}">Update</a></td>
                                <td><a href="{{ '/admin/catalogue/product/delete/'.$row->id }}"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5"></td>
                                <td><a href="{{ '/admin/catalogue/product/insert' }}">Insert</a></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>                                 
    </div>
</div>
@endsection