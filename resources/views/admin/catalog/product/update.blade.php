@extends('master')

@section('container')
<div class="container-fluid">
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Update<a class="float-right" href="/admin/catalogue/product/list">Retour</a></h2>
                </div>
                <div class="col-md-12">
                    <form action="{{ '/admin/catalogue/product/edit/'.$rows[0]->id }}" method="POST" enctype="application/form-data">
                        <fieldset>
                            <legend>Admin product</legend>
                            <div class="form-row">
                                <label for="id_categorie">Categorie</label>
                                <select name="id_categorie">
                                    @foreach ($categories as $val)
                                    @ {{ $select = $rows[0]->id_categorie == $val['id'] ? ' selected=selected' : '' }}
                                    <option value="{{ $val['id'] }}" {{ $select }}>{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row">
                                <label for="id_marque">Marque</label>
                                <select name="id_marque">
                                    @foreach ($marques as $val)
                                    @ {{ $select = $rows[0]->id_marque == $val['id'] ? ' selected=selected' : '' }}
                                    <option value="{{ $val['id'] }}" {{ $select }}>{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row">
                                <label for="id_livraison">Livraison</label>
                                <select name="id_livraison">
                                    @foreach ($livraisons as $val)
                                    @ {{ $select = $rows[0]->id_livraison == $val['id'] ? ' selected="selected"' : '' }}
                                    <option value="{{ $val['id'] }}" {{ $select }}>{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row">
                                <label for="id_paiement">Paiement</label>
                                <select name="id_paiement">
                                    @foreach ($paiements as $val)
                                    @ {{ $select = $rows[0]->id_paiement == $val['id'] ? ' selected="selected"' : '' }}
                                    <option value="{{ $val['id'] }}" {{ $select }}>{{ $val['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <label for="rate">Note</label>
                                <select name="rate">
                                    @foreach ([1,2,3,4,5] as $val)
                                    @ {{ $select = $rows[0]->rate == $val ? ' selected="selected"' : '' }}
                                    <option value="{{ $val }}" {{ $select }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="{{ $rows[0]->name }}">
                            </div>
                            <div class="form-row">
                                <label for="presentation">Presentation</label>
                                <textarea name="presentation">{{ $rows[0]->presentation }}</textarea>
                            </div>
                            <div class="form-row">
                                <label for="description">Description</label>
                                <textarea name="description" >{{ $rows[0]->description }}</textarea>
                            </div>
                            <div class="form-row">
                                <label for="garantie">Garantie</label>
                                <textarea name="garantie">{{ $rows[0]->garantie }}</textarea>
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <label for="prix_barre">Prix barre</label>
                                <input type="text" name="prix_barre" value="{{ $rows[0]->prix_barre }}">
                            </div>
                            <div class="form-row">
                                <label for="prix_vente">Prix vente</label>
                                <input type="text" name="prix_vente" value="{{ $rows[0]->prix_vente }}">
                            </div>
                            <!-- ----------------------------------------------  -->
                            <div class="form-row">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="submit" class="btn btn-primary" value="Modifier">
                            </div>
                        </fieldset>
                    </form>
                    <form action="{{ '/admin/catalogue/product/images' }}" method="POST" enctype="multipart/form-data">
                        <fieldset>
                            <legend>Admin images</legend>
                            @foreach ($images as $key=>$img)
                            <div class="form-row">
                                <label for="{{ 'image-'.$key }}">{{ $img->filename }}</label>
                                <input type="file" name="image[]" id="{{ 'image-'.$key }}">
                                <input type="hidden" name="filename[]" value="{{ $img->filename }}">
                            </div>
                            @endforeach
                            <div class="form-row">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="submit" class="btn btn-primary" value="Modifier">
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection