@extends('master')

@section('javascript')
@endsection

@section('container')
<div class="container product-list">
    @foreach ($rows as $row)
    @if ($loop->odd)
    <div class="row">
        @endif
        <div class="col-md-6">	
            <div class="col-md-8">
                <a href="{{ '/catalogue/products/'.$row->id_categorie.'/product/'.$row->id }}" id="product-img-0" class="product-img">
                    <img src="{{ '/medias/catalog/'.$images[$row->id][0]->filename }}" alt="{{ $row->name }}" width="100%" />
                </a>
            </div>
            <div class="col-md-4 product">
                <h6 class="product-categorie">{{ $categories[$row->id_categorie]['name'] }}</h6>
                <h2 class="product-title">{{ $row->name }}</h2>
                <h3 class="product-marque">{{ $marques[$row->id_marque]['name'] }}</h3>
            </div>
        </div>
        @if ($loop->even)
    </div>
    @endif
    @endforeach
</div>
@stop
