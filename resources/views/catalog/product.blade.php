@extends('master')

@section('javascript')
<script src="/js/product.js"></script>
@stop

@section('container')
@foreach ($rows as $key=>$row)
<div class="container">	
    <!-- images -->
    <!-- ------------------------------------------------------- -->
    <div class="col-md-5">
        <div class="product col-md-12">
            <div id="product-img-0" class="product-img">
                <img src="{{ '/medias/catalog/'.$images[$row->id][0]->filename }}" alt="{{ $row->name }}" width="100%" />
            </div>
        </div>
        <div class="product col-md-12">
            @foreach ($images[$row->id] as $image)
            <a id="product-img-1" class="product-img col-md-3">
                <img src="{{ '/medias/catalog/'.$image->filename }}" alt="{{ $row->name }}" />
            </a>
            @endforeach
        </div>
    </div>
    <!-- infos -->
    <!-- ------------------------------------------------------- -->
    <div class="product col-md-7">
        <h6 class="product-categorie">{{ $categories[$row->id_categorie]['name'] }}</h6>
        <h2 class="product-title">{{ $row->name }}</h2>
        <h3 class="product-marque">{{ $marques[$row->id_marque]['name'] }}</h3>
        <h6 class="product-desc">{{ $row->presentation }}</h6>
    </div>
    <div class="product col-md-7">
        <div class="product-rating">
            @for ($i=1; $i<6; $i++)
            @if ($i<=$row->rate)
            <i class="fa fa-star gold"></i>
            @else
            <i class="fa fa-star-o"></i>
            @endif
            @endfor
        </div>
    </div>
    <div class="product col-md-7">
        <div class="product-price">
            <span class="prix-vente">
                <span class="prix">{{ $row->prix_vente }}</span>
                <span class="devise">&euro;</span>
            </span>
        </div>
        <div class="product-price">
            <span class="prix-barre">
                <span class="prix">{{ $row->prix_barre }}</span>
                <span class="devise">&euro;</span>
                <div class="barre"></div>
            </span>
        </div>
    </div>
    <div class="product col-md-7">
        @if ($row->stock>0)
        <div class="product-stock">{{ 'Il reste: '.$row->stock.' pièce(s)' }}</div>
        @else
        <div class="product-stock">{{ 'Indisponible à la vente' }}</div>
        @endif
    </div>
    <div class="product col-lg-2">
        <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="">
                    <span class="glyphicon glyphicon-minus"></span>
                </button>
            </span>
            <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
            <span class="input-group-btn">
                <button type="button" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">
                    <span class="glyphicon glyphicon-plus"></span>
                </button>
            </span>
        </div>
    </div>
    <div class="product col-md-7">
        <div class="btn-group cart">
            <button type="button" class="btn btn-success">
                Ajouter au panier 
            </button>
        </div>
        <div class="btn-group wishlist">
            <button type="button" class="btn btn-primary">
                Ajouter à la liste d'envie 
            </button>
        </div>
    </div>
    <!-- tabs -->
    <!-- ----------------------------------------------------------- -->
    <div class="col-md-7 product-info">
        <ul id="myTab" class="nav nav-tabs nav_tabs">
            <li class="active"><a href="#service-one" data-toggle="tab">DESCRIPTION</a></li>
            <li><a href="#service-two" data-toggle="tab">LIVRAISON</a></li>
            <li><a href="#service-three" data-toggle="tab">GARANTIES</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="service-one">
                <section class="product-info">
                    <p>{{ $row->description }}</p>
                </section>
            </div>
            <div class="tab-pane fade" id="service-two">
                <section class="product-info">
                    <h4>{{ $livraisons[$row->id_livraison]['name'] }}</h4>
                    <p>{{ $livraisons[$row->id_livraison]['informations'] }}</p>
                </section>
            </div>
            <div class="tab-pane fade" id="service-three">
                <section class="product-info">
                    <p>{{ $row->garantie }}</p>
                </section>
            </div>
        </div>
    </div>
</div>                                 
@endforeach
@stop
