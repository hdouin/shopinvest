<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogueTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_categorie')->unsigned()->index();
            $table->integer('id_marque')->unsigned()->index();
            $table->integer('id_livraison')->unsigned()->index();
            $table->integer('id_paiement')->unsigned()->index();
            $table->string('name');
            $table->string('presentation');
            $table->string('descriptiion');
            $table->string('garantie');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_categorie')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->foreign('id_marque')
                ->references('id')
                ->on('marques')
                ->onDelete('cascade');
            $table->foreign('id_livraison')
                ->references('id')
                ->on('livraisons')
                ->onDelete('cascade');
            $table->foreign('id_paiement')
                ->references('id')
                ->on('paiements')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogue');
    }

}
