<?php

use Illuminate\Support\Facades\Route;

// -----------------------------------------------------------------------------
// frontOffice
// -----------------------------------------------------------------------------

// home
Route::get('/', 'IndexController@getIndex');

// authentication
Auth::routes();
/*
Route::post('login', 'AuthController@login');
Route::get('register', 'AuthController@register');
Route::post('register', 'AuthController@register');
 */

// catalogue
Route::get('catalogue/products/{cat}/product/{id}', 'ProductController@getProduct');


// -----------------------------------------------------------------------------
// BackOffice
// -----------------------------------------------------------------------------

// catalogue
Route::get('admin/catalogue/product/list', 'CatalogueController@getAdminCatalogue');
Route::get('admin/catalogue/product/update/{id}', 'ProductController@getAdminProductUpdate');
Route::get('admin/catalogue/product/insert', 'ProductController@getAdminProductInsert');
Route::get('admin/catalogue/product/delete/{id}', 'ProductController@postAdminProductDelete');
Route::post('admin/catalogue/product/edit/{id}', 'ProductController@postAdminProductEdit');
Route::post('admin/catalogue/product/images', 'ProductController@postAdminProductUpdateImages');
Route::post('admin/catalogue/product/insert', 'ProductController@postAdminProductEdit');

