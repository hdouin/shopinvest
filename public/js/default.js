$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });
    $('#back-top').on('click',function () {
        $('#back-top').tooltip('hide');
        $('body,html').animate({scrollTop: 0}, 400);
        return false;
    });
    $('#back-top').tooltip('show');
    
    $('#btn-login').on('click',function() {
        $('#popup-login').show();
        $('#popup-login .close').on('click',function() {
            $('#popup-login').hide();
        });
    });
    
    $('#btn-register').on('click',function() {
        $('#popup-register').show();
        $('#popup-register .close').on('click',function() {
            $('#popup-register').hide();
        });
    });

});