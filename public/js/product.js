$(document).ready(function () {
    
    // switch images
    $('.product-img').on('click',function () {
        var old = $('#product-img-0 img').attr('src');
        var src = $(this).children('img').attr('src');
        $('#product-img-0 img').attr('src',src);
        $(this).attr('src',old);
    });
    
    // quantity product
    $('.quantity-right-plus').on('click',function (e) {
        e.preventDefault();
        var qty = parseInt($('#quantity').val());
        $('#quantity').val(qty + 1);
    });
    $('.quantity-left-minus').on('click',function (e) {
        e.preventDefault();
        var qty = parseInt($('#quantity').val());
        if (qty > 1) {
            $('#quantity').val(qty - 1);
        }
    });
    
    // add to cart
    $('.cart .btn-success').on('click',function (e) {
        e.preventDefault();
        var qty = parseInt($('#quantity').val());
        var prx = $('.prix-vente .prix').text();
        var prices = prx * qty;
        var articles = (qty > 1) ? ' article(s)' : ' article';
        $('.article').text(qty + ' ' + articles);
        $('.total').text(prices.toFixed(2));
    });
    
});