<?php

namespace App\Http\Controllers;

use App\Http\Controllers\NavigationController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\MarquesController;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function getLogin()
    {
        $navigation = NavigationController::getNavigation();
        $categories = CategoriesController::getAllCategories();
        $marques = MarquesController::getAllMarques();
        return view('home/index', [
            'navigation' => $navigation,
            'categories' => $categories,
            'marques' => $marques,
        ]);
    }

    public function postLogin(Request $request)
    {
        return 'Le nom est ' . $request->input('nom'); 
    }

}
