<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MarquesController extends Controller
{

    public static function getAllMarques()
    {
        $rows = [];
        $db_rows = DB::table('marques')->get();
        foreach ($db_rows as $row):
            $rows[$row->id]['id'] = $row->id;
            $rows[$row->id]['name'] = $row->name;
        endforeach;
        return $rows;
    }
    
}
