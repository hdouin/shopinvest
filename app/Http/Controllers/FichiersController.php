<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FichiersController extends Controller
{

    public function store(Request $request)
    {
        $request->validate([
            'filemane' => 'required:max:255',
        ]);
        auth()->user()->files()->create([
            'filename' => $request->get('filename'),
        ]);
        return back()->with('message', 'Submitted Successfully');
    }

}
