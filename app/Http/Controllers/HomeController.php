<?php

namespace App\Http\Controllers;

use App\Http\Controllers\NavigationController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\MarquesController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $navigation = NavigationController::getNavigation();
        $categories = CategoriesController::getAllCategories();
        $marques = MarquesController::getAllMarques();
        return view('home/index', [
            'navigation' => $navigation,
            'categories' => $categories,
            'marques' => $marques,
        ]);
    }
}
