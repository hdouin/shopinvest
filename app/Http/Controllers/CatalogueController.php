<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\NavigationController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\MarquesController;
use App\Http\Controllers\LivraisonsController;
use App\Http\Controllers\PaiementsController;

class CatalogueController extends Controller
{
    
    public static function getAdminCatalogue()
    {
        $navigation = NavigationController::getNavigation();
        $categories = CategoriesController::getAllCategories();
        $marques = MarquesController::getAllMarques();
        $livraisons = LivraisonsController::getAllLivraisons();
        $paiements = PaiementsController::getAllPaiements();
        $rows = DB::table('catalogue')->get();
        return view('admin/catalog/product/list', [
            'navigation' => $navigation,
            'categories' => $categories,
            'marques' => $marques,
            'livraisons' => $livraisons,
            'paiements' => $paiements,
            'rows' => $rows
        ]);
    }
    
}
