<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PaiementsController extends Controller
{

    public static function getAllPaiements()
    {
        $rows = [];
        $db_rows = DB::table('paiements')->get();
        foreach ($db_rows as $row):
            $rows[$row->id]['id'] = $row->id;
            $rows[$row->id]['name'] = $row->name;
        endforeach;
        return $rows;
    }
    
}
