<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LivraisonsController extends Controller
{

    public static function getAllLivraisons()
    {
        $rows = [];
        $db_rows = DB::table('livraisons')->get();
        foreach ($db_rows as $row):
            $rows[$row->id]['id'] = $row->id;
            $rows[$row->id]['name'] = $row->name;
            $rows[$row->id]['informations'] = $row->informations;
        endforeach;
        return $rows;
    }
    
}
