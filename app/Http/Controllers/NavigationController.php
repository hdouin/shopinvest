<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NavigationController extends Controller
{

    public static function getNavigation()
    {
        $rows = [];
        $parent = DB::table('categories')->get();
        foreach ($parent as $cat):
            $rows[$cat->id]['id'] = $cat->id;
            $rows[$cat->id]['name'] = $cat->name;
            $rows[$cat->id]['children'] = DB::table('catalogue')->select('id','name')->where('id_categorie',$cat->id)->get();
        endforeach;
        return $rows;
    }
    
}
