<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{

    public static function getAllCategories()
    {
        $rows = [];
        $db_rows = DB::table('categories')->get();
        foreach ($db_rows as $row):
            $rows[$row->id]['id'] = $row->id;
            $rows[$row->id]['name'] = $row->name;
        endforeach;
        return $rows;
    }
    
}
