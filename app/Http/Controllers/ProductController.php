<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\NavigationController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\LivraisonsController;
use App\Http\Controllers\MarquesController;
use App\Http\Controllers\PaiementsController;

class ProductController extends Controller
{

    /*
     * Route /catalogue/products/cat/product/id
     * @param int $cat
     * @param int $id
     */
    public function getProduct($cat,$id)
    {
        $navigation = NavigationController::getNavigation();
        $categories = CategoriesController::getAllCategories();
        $marques = MarquesController::getAllMarques();
        $livraisons = LivraisonsController::getAllLivraisons();
        $paiements = PaiementsController::getAllPaiements();
        $rows = DB::table('catalogue')
            ->where([['id',$id],['id_categorie',$cat]])
            ->get();
        $images = [];
        foreach ($rows as $item):
            $images[$item->id] = DB::table('fichiers')
                ->select('filename')
                ->where('id_catalogue',$item->id)
                ->get();
        endforeach;
        return view('catalog/product', [
            'navigation' => $navigation,
            'categories' => $categories,
            'marques' => $marques,
            'livraisons' => $livraisons,
            'paiements' => $paiements,
            'rows' => $rows,
            'images' => $images,
        ]);
    }
    
    /* ********************************************************************** */

    /*
     * Route /admin/catalogue/product/update/id
     * @param int $id
     */
    public function getAdminProductUpdate($id)
    {
        $navigation = NavigationController::getNavigation();
        $categories = CategoriesController::getAllCategories();
        $marques = MarquesController::getAllMarques();
        $livraisons = LivraisonsController::getAllLivraisons();
        $paiements = PaiementsController::getAllPaiements();
        $rows = DB::table('catalogue')->where([['id',$id]])->get();
        $images = [];
        foreach ($rows as $item):
            $images = DB::table('fichiers')
                ->select('id','filename')
                ->where('id_catalogue',$item->id)
                ->get();
        endforeach;
        return view('admin/catalog/product/update', [
            'navigation' => $navigation,
            'categories' => $categories,
            'marques' => $marques,
            'livraisons' => $livraisons,
            'paiements' => $paiements,
            'rows' => $rows,
            'images' => $images,
        ]);
    }

    /*
     * Route /admin/catalogue/product/insert
     */
    public function getAdminProductInsert()
    {
        $navigation = NavigationController::getNavigation();
        $categories = CategoriesController::getAllCategories();
        $marques = MarquesController::getAllMarques();
        $livraisons = LivraisonsController::getAllLivraisons();
        $paiements = PaiementsController::getAllPaiements();
        return view('admin/catalog/product/insert', [
            'navigation' => $navigation,
            'categories' => $categories,
            'marques' => $marques,
            'livraisons' => $livraisons,
            'paiements' => $paiements,
        ]);
    }

    /* ********************************************************************** */

    /*
     * Route /admin/catalogue/product/edit/id
     * @param int $id
     * @Request $request
     */
    public function postAdminProductEdit(Request $request,$id=null)
    {
        $datas = [
            'id_categorie' => $request->input('id_categorie'),
            'id_marque' => $request->input('id_marque'),
            'id_livraison' => $request->input('id_livraison'),
            'id_paiement' => $request->input('id_paiement'),
            'name' => $request->input('name'),
            'presentation' => $request->input('presentation'),
            'description' => $request->input('description'),
            'garantie' => $request->input('garantie'),
            'prix_barre' => $request->input('prix_barre'),
            'prix_vente' => $request->input('prix_vente'),
            'rate' => $request->input('rate'),
            'stock' => $request->input('stock'),
        ];
        if ($id):
            DB::table('catalogue')->where('id',$id)->update($datas);
            $msg = 'Updated successfully.';
        else:
            $id = DB::table('catalogue')->insertGetId($datas);
            DB::table('fichiers')->insert(['id_catalogue'=>$id, 'filename'=>time().'-1.jpg']);
            DB::table('fichiers')->insert(['id_catalogue'=>$id, 'filename'=>time().'-2.jpg']);
            DB::table('fichiers')->insert(['id_catalogue'=>$id, 'filename'=>time().'-3.jpg']);
            $msg = 'Updated successfully.';
        endif;
        return redirect('/admin/catalogue/product/list')->with('message',$msg);
    }

    /*
     * Route /admin/catalogue/product/delete/id
     * @param int $id
     * @Request $request
     */
    public function postAdminProductDelete($id)
    {
        DB::table('catalogue')->where('id',$id)->delete();
        DB::table('fichiers')->where('id_catalogue',$id)->delete();
        $msg = 'Updated successfully.';
        return redirect('/admin/catalogue/product/list')->with('message',$msg);
    }
    
    /*
     * Route /admin/catalogue/product/image/id
     * @param int $id
     * @Request $request
     */
    public function postAdminProductUpdateImages(Request $request)
    {
        $filename = $request->input('filename');
        $images = $request->file('image');
        foreach ($images as $key=>$img):
            $file = $filename[$key];
            $path = public_path().'/medias/catalog/';
            $img->move($path, $file);
        endforeach;
        $msg = 'Updated successfully.';
        return redirect('/admin/catalogue/product/list')->with('message',$msg);
    }
    
}
