<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\NavigationController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\MarquesController;

class IndexController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }
//
    /*
     * Route /
     */
    public function getIndex()
    {
        $navigation = NavigationController::getNavigation();
        $categories = CategoriesController::getAllCategories();
        $marques = MarquesController::getAllMarques();
        $rows = DB::table('catalogue')
            ->select('catalogue.id','catalogue.id_categorie','catalogue.id_marque','catalogue.name')
            ->get();
        $images = [];
        foreach ($rows as $item):
            $images[$item->id] = DB::table('fichiers')
                ->select('filename')
                ->where('id_catalogue',$item->id)
                ->limit(1)
                ->get();
        endforeach;
        return view('home/index', [
            'navigation' => $navigation,
            'categories' => $categories,
            'marques' => $marques,
            'rows' => $rows,
            'images' => $images,
        ]);
    }    

}
