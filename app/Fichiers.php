<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fichiers extends Model
{
    
    use SoftDeletes;

    protected $fillable = [
      'filename',
    ];

    public function catalogue()
    {
      return $this->belongsTo(Catalogue::class);
    }
    
}
